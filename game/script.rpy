﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
image hearthstone = "hearthstone.png"

# Pedra
image pedra bored = "pedra_bored.png"

# Venn
image venn show = "venn_show.png"

# Declare characters used by this game.
define p = Character('Pedra', color="#c8ffc8")
define v = Character('Venn', color="#c8ffc8")
define j = Character('Jordan', color="#c8ffc8")
define e = Character('__eMpTy__', color="#c8ffc8")
define o = Character('Orn', color="#c8ffc8")
define h = Character('Hearthstone', color="#c8ffc8")


# The game starts here.
label start:

    show pedra bored at right
    p "Sigh..."
    
    show hearthstone
    play sound "jobsdone.ogg"
    h "Job's done."

    show pedra bored at right
    p "The show is almost on, again..."

    show venn show at right
    v "AAAAAAAND WELCOME BACK to another Linux Game Cast Weekly, the show that covers the latest Linux gaming news, reviews, how-to's and most importantly, whatever the hell, elks, we, come, up, with!  This week..."

    menu:
      "Selection 1":
        jump one
      "Selection 2":
        jump two

label one:

label two:

    return
